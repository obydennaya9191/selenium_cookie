package com.mycompany.l03.capabilities;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.io.IOException;

public enum Browsers {
    FIREFOX("firefox"), CHROME("chrome");

    private String browser;

    Browsers(String browser) {
        this.browser = browser;
    }

    @Override
    public String toString() {
        return browser;
    }

    //get browser settings from a file
    public String getBrowserProperty(String name) {

        try (InputStream input = new FileInputStream("src/test/resources/browser.properties")) {

            Properties property = new Properties();
            property.load(input);
            String value = property.getProperty(name);

            return value;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
